package udesc.cct.poo.passagem.controle;

import java.util.Scanner;

import udesc.cct.poo.passagem.servicos.ServicoDeLocais;
import udesc.cct.poo.passagem.servicos.ServicoDePassagens;
import udesc.cct.poo.passagem.servicos.ServicoDeViagens;

public class ControleGeral {
	public final static int MARCAR_PASSAGEM = 1;
	public final static int SAIR = 0;
	public final static int LISTAR_VIAGENS = 2;

	private Scanner scanner;
	private ServicoDeLocais servicoDeLocais;
	private ServicoDeViagens servicoDeViagens;
	private ServicoDePassagens servicoDePassagens;
	private ControleMarcarPassagem marcarPassagem;
	private ControledeViagens controleDeViagens;

	public ControleGeral() {
		this.scanner = new Scanner(System.in);
		this.setServicoDeLocais(new ServicoDeLocais());
		this.setServicoDeViagens(new ServicoDeViagens());
		this.setServicoDePassagens(new ServicoDePassagens());
		this.setMarcarPassagem(new ControleMarcarPassagem(this.scanner, this.getServicoDeLocais(), this.getServicoDeViagens(), this.getServicoDePassagens()));
	}
	
	public ServicoDeLocais getServicoDeLocais() {
		return this.servicoDeLocais;
	}
	
	public void setServicoDeLocais(ServicoDeLocais servicoDeLocais) {
		this.servicoDeLocais = servicoDeLocais;
	}
	
	public ServicoDeViagens getServicoDeViagens() {
		return this.servicoDeViagens;
	}
	
	public void setServicoDeViagens(ServicoDeViagens servicoDeViagens) {
		this.servicoDeViagens = servicoDeViagens;
	}
	
	public ServicoDePassagens getServicoDePassagens() {
		return this.servicoDePassagens;
	}
	
	public void setServicoDePassagens(ServicoDePassagens servicoDePassagens) {
		this.servicoDePassagens = servicoDePassagens;
	}
	
	public ControleMarcarPassagem getMarcarPassagem() {
		return this.marcarPassagem;
	}
	
	public void setMarcarPassagem(ControleMarcarPassagem marcarPassagem) {
		this.marcarPassagem = marcarPassagem;
	}
	
	public ControledeViagens getControleDeViagens() {
		return this.controleDeViagens;
	}
	
	public void setControleDeViagens(ControledeViagens controleDeViagens) {
		this.controleDeViagens = controleDeViagens;
	}

	public void iniciar() {
		int escolha = -1;
		
		while (escolha != ControleGeral.SAIR) {
			System.out.println("Escolha algo:");
			System.out.println(ControleGeral.MARCAR_PASSAGEM + ") Marcar passagem");
			System.out.println(ControleGeral.SAIR + ") Sair");
			System.out.println(ControleGeral.LISTAR_VIAGENS + ") Listar Viagens");
			
			escolha = this.scanner.nextInt();

			switch (escolha) 
			{
				case ControleGeral.MARCAR_PASSAGEM:
					this.marcarPassagem.iniciar();
					break;
					
				case ControleGeral.LISTAR_VIAGENS:
					this.controleDeViagens.listar();
					break;
				default:
					break;
			}
		}
	}
}
