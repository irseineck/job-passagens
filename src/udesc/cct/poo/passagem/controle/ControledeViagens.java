package udesc.cct.poo.passagem.controle;

import java.util.ArrayList;

import udesc.cct.poo.passagem.modelos.Assento;
import udesc.cct.poo.passagem.modelos.Parada;
import udesc.cct.poo.passagem.modelos.Viagem;
import udesc.cct.poo.passagem.servicos.ServicoDeViagens;

public class ControledeViagens {

	private ServicoDeViagens servicoDeViagens;
	private ArrayList<Viagem> viagens;

	public ControledeViagens(ServicoDeViagens servicoDeViagens) {

		this.servicoDeViagens = servicoDeViagens;

	}

	public void listar() {
		this.getViagens();
		for (int i = 0; i < this.viagens.size(); i++) {
			int idx = i + 1;
			Viagem v = viagens.get(i);
			ArrayList<Parada> paradas = new ArrayList<Parada>();
			paradas = v.getParadasporViagem();
			System.out.println("Viagem " + idx + "-");
			for (int j = 0; j < paradas.size(); j++) {
				int indice = j + 1;
				System.out.println("Parada " + indice + " - " + paradas.get(j).getInfo());
			}
			System.out.println();

		}
		System.out.println("Viagens com assentos leito");
		for (int i = 0; i < this.viagens.size(); i++) {
			int idx = i + 1;
			Viagem v = viagens.get(i);
			ArrayList<Parada> paradas = new ArrayList<Parada>();
			paradas = v.getParadasporViagem();
			ArrayList<Assento> assentos = new ArrayList<Assento>();
			assentos = v.getAssentos();
			int assentoInt = 100;
			for (int k = 0; k < assentos.size(); k++) {
				Assento assento = assentos.get(k);
				assentoInt = assento.getTipo();
				if (assentoInt == 0)
					break;
			}

			if (assentoInt == 0) {
				System.out.println("Viagem " + idx + "-");
				for (int j = 0; j < paradas.size(); j++) {
					int indice = j + 1;
					System.out.println("Parada " + indice + " - " + paradas.get(j).getInfo());
				}
				System.out.println();
			}

		}
	}

	private void getViagens() {
		this.viagens = servicoDeViagens.getTodosAsViagens();
	}

}
