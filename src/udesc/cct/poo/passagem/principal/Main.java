package udesc.cct.poo.passagem.principal;

//import udesc.cct.poo.passagem.controle.ControleGeral;
import udesc.cct.poo.passagem.controle.ControleGeral;

import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.codec.digest.DigestUtils;

public class Main {
	public static void main(String[] args) {
		boolean statusLogin = false;
		String senhaDigitada;
		String nomeDigitado;
		
		ArrayList<Usuario> usuarios = new ArrayList<Usuario>(2);
		
		Usuario usuario1 = new Usuario();
		Usuario usuario2 = new Usuario();
		
		usuario1.nome = "pedro";
		usuario2.nome = "jose";
		
		usuario1.senha = DigestUtils.sha1Hex("teste");
		usuario2.senha = DigestUtils.sha1Hex("teste2");
		
		usuarios.add(usuario1);
		usuarios.add(usuario2);
		
		Scanner scan = new Scanner(System.in); 
		System.out.println("Digite seu nome: ");
		
		nomeDigitado = scan.nextLine();
		System.out.println("Digite sua senha: ");
		senhaDigitada = scan.nextLine();
		
	
		
		for (Usuario usuario : usuarios) {
	         
			if (DigestUtils.sha1Hex(senhaDigitada).equals(usuario.senha) && usuario.nome.equals(nomeDigitado))
			{
				statusLogin = true;
			}
			
	    }
		
		if (statusLogin)
		{
			System.out.println("logado com Sucesso!");
			ControleGeral controleGeral = new ControleGeral();
			controleGeral.iniciar();
		}
		else
		{
			System.out.println("Senha ou usu�rio errados");
		}
	}
}

class Usuario {
	String nome;
	String senha;
}